import React,{useState, useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';

const Post = ({post:{id, title, content, comments, likes},
   handleAddLikes, 
   handleAddComments,
   userId}) =>{

  const [currentLikes, setCurrentLikes] = useState(likes);
  const [currentComments, setCurrentComments] = useState(comments);
  const [currentComment, setCurrentComment] = useState('');

  const addLikes = () =>{
    setCurrentLikes(currentLikes + 1);
    handleAddLikes(id);
  }

  const handleAddComment = (id) =>{
    const newComment = {userId, content: currentComment};
    setCurrentComments([...currentComments, newComment]);
    handleAddComments(id, newComment);
    setCurrentComment('');
  }

  return (
      <Card elevation={5} style={{margin: '10px'}}>
        <CardContent>
        <Grid xs={12} container>
          <Grid item xs={11} style={{textAlign: 'left'}}>{title}</Grid>    
          <Grid item xs={1}>
            {currentLikes}
            <img src="./img/like.png" alt="like" onClick={() => addLikes()}/>
          </Grid>
          <Grid item style={{textAlign: 'left'}}>{content}</Grid>
          <Grid item xs={12}>
            <TextField fullWidth variant="outlined" id="comment" label="Add comment" value={currentComment} onChange={(e) => setCurrentComment(e.target.value)}/>
            <button onClick={() => handleAddComment(id)}>add comment</button>
          </Grid>
          Comments
          {
            currentComments.map(comment => (<Grid style={{fontStyle: 'italic'}} item xs={12}>{comment.content}</Grid>))          
          }
        </Grid>
      </CardContent>
    </Card>
  )
}


export default Post;
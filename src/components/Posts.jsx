import React from 'react';

import Post from './Post';

const Posts = ({posts, handleAddLikes, handleAddComments, userId}) =>{
  return (
    <div>
      {
        posts.map(post => (
          <Post 
            userId={userId}
            post={post} 
            handleAddLikes={(postId) => handleAddLikes(postId)}
            handleAddComments={(postId,comment) => handleAddComments(postId, comment)}/>
        ))
      }
    </div>
  );
}


export default Posts;
import React, {useState, useEffect} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Posts from './Posts';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const Painel = () =>{

  const userId = 123456; // to emulate local user
  const [newPostsExists, setNewPostsExists] = useState(false);
  const [value, setValue] = useState(0);
  const [totalLikes, setTotalLikes] = useState(0);
  const [totalComments, setTotalComments] = useState(0);
  const [posts, setPosts] = useState([
    {
      id: Math.floor(100000 + Math.random() * 900000),
      title: 'Great news!',
      content: ' Nullam massa lectus, pellentesque rhoncus vestibulum nec, efficitur eu lectus. Suspendisse potenti. Fusce euismod porta mollis. Nulla quis iaculis libero, a rutrum diam. Integer condimentum faucibus neque id porta. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos',
      likes: 0,
      comments: [
        {
          userId: Math.floor(100000 + Math.random() * 900000),
          content: 'spendisse potenti. Fusce euismod porta mollis. Nulla quis iaculis libero, a rutrum diam. Integer condimentum'
        },{
          userId: Math.floor(100000 + Math.random() * 900000),
          content: 'c dignissim dolor quis urna convallis, id mollis libero '
        }
      ]
    },
    {
      id: Math.floor(100000 + Math.random() * 900000),
      title: 'Another post!',
      content: ', gravida nisi eget, vulputate nisi. Nam eget maximus orci. Proin mauris quam, gravida sed iaculis nec, aliquet vitae quam. Pellentesque ornare, quam et cursus dapibus, tortor purus volutpat urna, elementum pretium metus neque ac elit. Nullam placerat arcu nec iaculis ornare. Donec',
      likes: 0,
      comments: [
        {
          userId: Math.floor(100000 + Math.random() * 900000),
          content: 'raesent ut mattis odio. Vestibulum mauris lectus, ultrices at gravida nec, euismod ac arcu. Proin ultrices aliquam elit et facilisis. Sed eget molestie ligula. Morbi id sodales sapien. Pe'
        }
      ]
    }
  ]);

  setTimeout(()=>{//just to emulate a new post since I dont have a real update here 
    if(!newPostsExists){
      setNewPostsExists(true);
    }
  }, 5000);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const addTotalLikes = (postId) =>{
    const changedPosts = posts.map(post => {
      if(post.id === postId){
        post.likes = post.likes + 1
      }
      return post;
    })

    setPosts(changedPosts);
    sumLikesAndComments();
  }

  const addComment = (postId, comment) => {
    const changedPosts = posts.map(post => {
      if(post.id === postId){
        post.comments = [...post.comments, comment];
      }
      return post;
    })

    setPosts(changedPosts);
    sumLikesAndComments();
  }

  const sumLikesAndComments = () =>{
    const totalLikes = posts.reduce((p0, p1) => (p0.likes + p1.likes));
    setTotalLikes(totalLikes);
    console.log(posts);
    const totalComments = posts.reduce((p0, p1) => (p0.comments.length + p1.comments.length));
    setTotalComments(totalComments);
  }

  const reloadPosts = () => {
    const newPost =  {
      id: Math.floor(100000 + Math.random() * 900000),
      title: 'New post!',
      content: ' Nullam massa lectus, pellentesque rhoncus vestibulum nec, efficitur eu lectus. Suspendisse potenti. Fusce euismod porta mollis. Nulla quis iaculis libero, a rutrum diam. Integer condimentum faucibus neque id porta. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos',
      likes: 0,
      comments: []
    };
    setPosts([...posts, newPost])
  }

  useEffect(() => {
    sumLikesAndComments()
  }, []);

  return (
    <div>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Posts" />
          <Tab label="Comments" />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <AppBar position="static">
          <Grid container>
            <Grid item xs={2} >
              {
                newPostsExists?(
                  <button onClick={()=>reloadPosts()}>reload posts</button>
                ):
                ''
              }
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item container xs={4} alignContent="space-between">
              <Grid item xs={6}>Likes: {totalLikes}</Grid>
              <Grid item xs={6}>Comments: {totalComments}</Grid>
            </Grid>
          </Grid>
        </AppBar>
        <Posts 
          userId={userId}
          posts={posts} 
          handleAddLikes={(postId)=>addTotalLikes(postId)}
          handleAddComments={(postId, comment)=>addComment(postId, comment)}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        {
          posts.filter(post=> post.comments.filter(c => c.userId === userId).length > 0).map(post => (
            <Grid containter>
              <Grid item>{post.title}</Grid>
              {
                post.comments.filter(c => c.userId === userId).map(comment => (
                  <Grid item>{comment.content}</Grid>
                ))
              }
            </Grid>
          ))
        }
      </TabPanel>
    </div>
  )
}

export default Painel;
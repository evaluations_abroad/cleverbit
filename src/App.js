import React from 'react';
import logo from './logo.svg';
import './App.css';

import Panel from './components/Painel';

function App() {
  return (
    <div className="App">
      <Panel/>
    </div>
  );
}

export default App;
